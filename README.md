## Official Minecraft Modpack for Patriam
This is the official modpack for the server Patriam: Infinitus. This modpack is specifically designed for server use and there have been reports of issues with singleplayer.
Singleplayer is not supported officially and thus issues will not be investigated or fixed unless they are also present on the server.

The server will also run on the 'stable' branch of this repository. This branch is updated less frequently as the changes are downloaded by clients automatically.
All other branches are considered unstable and may contain bugs or not work at all. They are for development purposes only.

### Discord
Join our Discord server to get the latest news and updates about the server and modpack. [https://discord.patriam.cc/](https://discord.patriam.cc/)

### Server Information
The server IP is `play.patriam.cc` but `mc.patriam.cc` or `patriam.cc` will also work.