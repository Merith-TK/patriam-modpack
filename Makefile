default:
	@echo "No Default make command configured"
	@echo "Please use either"
	@echo "   - make curseforge"
	@echo "   - make multimc"
	@echo "   - make technic"
	@echo "   - make all"
	@echo ""
	@echo "   - make server"
	@echo ""
	@echo "Curseforge will make a curseforge compatible zip"
	@echo "Multimc will make a multimc zip file which contains"
	@echo "   the packwiz updater"
	@echo ""
	@echo "Technic will make a technic pack zip"
	@echo ""
	@echo "All will make all packs it can"
	@echo ""
	
curseforge:
	@echo "[Platform Build] Creating CurseForge ZIP"
	packwiz curseforge export --pack-file .minecraft/pack.toml -o build/patriam-curseforge.zip
	7z d build/patriam-curseforge.zip overrides/packwiz-installer-bootstrap.jar overrides/pack.toml  overrides/index.toml overrides/bin/modpack.jar	
	@echo "Done"
	
multimc:
	@echo "[Platform Build] Creating MultiMC ZIP"
	7z d build/patriam-multimc.zip ./* -r
	7z d build/patriam-multimc.zip ./.minecraft -r
	7z a build/patriam-multimc.zip ./* -r
	7z a build/patriam-multimc.zip ./.minecraft -r
	7z d build/patriam-multimc.zip ./.minecraft/mods ./.minecraft/bin ./.minecraft/pack.toml ./.minecraft/index.toml ./build -r

technic: clean
	@echo "[Platform Build] Creating Technic ZIP"
	-cp -r .minecraft .technic
	mv .technic/patriam_icon.png .technic/icon.png
	cd .technic && java -jar packwiz-installer-bootstrap.jar ../.minecraft/pack.toml && cd ..
	-rm -rf .technic/packwiz* .technic/pack.toml .technic/index.toml .technic/mods/*.pw.toml
	7z d build/patriam-technic.zip ./* -r
	7z a build/patriam-technic.zip ./.technic/* -r

server:
	@echo "[Platform Build] Creating Server Files"
	-cp .minecraft/packwiz-installer-bootstrap.jar .server/packwiz-installer-bootstrap.jar
	rm -rf ./.server./config/bcc-common.toml
	cd .server && java -jar packwiz-installer-bootstrap.jar -g -s server ../.minecraft/pack.toml
	cd .server && rm -rf ./mods/WaveyCapes-forge-1.2.0-mc1.16.5.jar

clean:
	-rm -rf build/*
	-rm -rf .technic
	-git gc --aggressive --prune

clean-server:
	-rm -rf .server/config
	-rm -rf .server/crash-reports
	-rm -rf .server/logs
	-rm -rf .server/mods
	-rm -rf .server/world
	echo "eula=true" > .server/eula.txt

clean-libs: clean
	-rm -rf .server/libraries

all: clean server multimc technic curseforge

update-packwiz:
	go install github.com/packwiz/packwiz@latest
	go install github.com/Merith-TK/packwiz-wrapper/cmd/pw@latest
	clear
	@echo "Packwiz has been Updated"
